<?php

include_once("vendor/autoload.php");
include_once('lib/app.php');

use OroCoder\Crud\Category;

$category = new Category();
$id = $_POST['id'];
$name = $_POST['name'];
$description = $_POST['description'];
$created = $_POST['created'];
$modified = $_POST['modified'];
$created_by = $_POST['created_by'];
$modified_by = $_POST['modified_by'];
$deleted_at = $_POST['deleted_at'];
$category->create($id, $name, $description, $created, $modified, $created_by, $modified_by, $deleted_at);
?>

