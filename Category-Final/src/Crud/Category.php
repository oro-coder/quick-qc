<?php

namespace OroCoder\Crud;

use OroCoder\Utility;

class Category {
    protected $id='';
    protected $name = '';
    protected $description = '';
    protected $created = '';
    protected $modified = '';
    protected $created_by = '';
    protected $modified_by = '';
    protected $deleted_at = '';
    protected $data = array();
    
    private $conn = "";

    public function __construct() {
        $this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function all() {

        try {
            $query = "SELECT * FROM categories";

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function create($id = null,$name , $description ,$created ,$modified ,$created_by,$modified_by,$deleted_at) {

        try {
            $this->name = $name;
            $this->description = $description;
            $thsi->created = $created;
            $this->modified = $modified;
            $this->created_by = $created_by;
            $this->modified_by = $modified_by;
            $this->deleted_at = $deleted_at;
            
            $query = "INSERT INTO categories (name,description,created,modified,created_by,modified_by,deleted_at) VALUES (:name,:description,:created,:modified,:created_by,:modified_by,:deleted_at)";
            $stmt =$this->conn->prepare($query);
            $stmt->execute(array(
                               
                              ':name'=> $this->name,
                              ':description' => $this->description,
                              ':created' => $this->created,
                              ':modified' => $this->modified,
                              ':created_by' => $this->created_by,
                              ':modified_by' => $this->modified_by,
                              ':deleted_at' => $this->deleted_at)
            );

            Utility::redirect();
        }catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function get($id = NULL) {
        try{
            $this->id = $id;
        $query = "SELECT * FROM categories WHERE id = $this->id ";
        echo $query;

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;

    }

    public function store($name,$description,$created,$modified,$created_by,$modified_by,$deleted_at,$id) {
            
            $this->name = $name;
            $this->description = $description;
            $this->created = $created;
            $this->modified = $modified;
            $this->created_by = $created_by;
            $this->modified_by = $modified_by;
            $this->deleted_at = $deleted_at;
            $this->id = $id;
             $sql = "UPDATE `categories` SET `name` = :name, `description` = :description, `created` = :created, modified = :modified ,`created_by` = :created_by, `modified_by` = :modified_by, `deleted_at` = :deleted_at WHERE `id` = :id ";

        $stmt =$this->conn->prepare($sql);


        $stmt->bindParam(':name', $name,\PDO::PARAM_STR);
        $stmt->bindParam(':description',$description,\PDO::PARAM_STR);
        $stmt->bindParam(':created',$created,\PDO::PARAM_INT);
        $stmt->bindParam(':modified',$modified,\PDO::PARAM_INT);
        $stmt->bindParam(':created_by', $created_by,\PDO::PARAM_INT);
        $stmt->bindParam(':modified_by',$modified_by,\PDO::PARAM_INT);
        $stmt->bindParam(':deleted_at',$deleted_at,\PDO::PARAM_INT);
        $stmt->bindParam(':id', $id,\PDO::PARAM_INT);
        
        if($stmt->execute()){header("location: manage_category.php");}
        
        
        }
    

   public function delete($id = NULL) {
        
       try {
        $this->id = $id;

        $stmt = $this->conn->prepare('DELETE FROM categories WHERE id = :id');
        $stmt->execute(array('id' => $this->id));
    } 
                
     catch (PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }
header("location: manage_category.php");
}

}