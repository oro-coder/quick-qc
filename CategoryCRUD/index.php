<?php
include_once("vendor/autoload.php");
include_once('lib/app.php');

use OroCoder\Crud\Category;

$category = new Category();
$profiles = $category->all();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head> 
    <body>
        <p><a href="create.html">Click Here</a> to add new category</p>
        
        |Search Category|
        |Evaluate Category | Get Category Status |
        <table border="1">
            <tr>
                <th>Sl</th>
                <th>Name</th>
                <th>Description</th>
                <th>Created</th>
                <th>Modified</th>
                <th>Created By</th>
                <th>Modified By</th>
                <th>Deleted At</th>
                <th>Action</th>
            </tr>
           <?php
           
           if( !empty($profiles)){
           foreach($profiles as $key=>$value){
               
          ?>    
           
            <tr>
                <td><?php echo $key+1;?></td>
                <td><?php 
                if(array_key_exists('name', $value) && !empty($value['name'])){
                    echo $value['name'];
                }
                  ?></td>
                <td><?php
                if(array_key_exists('description', $value) && !empty($value['description'])){
                    echo $value['description'];
                }
                ?></td>
                <td><?php 
                if(array_key_exists('created', $value) && !empty($value['created'])){
                    echo $value['created'];
                }
                  ?></td>
                <td><?php
                if(array_key_exists('modified', $value) && !empty($value['modified'])){
                    echo $value['modified'];
                }
                ?></td>
                <td><?php 
                if(array_key_exists('created_by', $value) && !empty($value['created_by'])){
                    echo $value['created_by'];
                }
                  ?></td>
                <td><?php
                if(array_key_exists('modified_by', $value) && !empty($value['modified_by'])){
                    echo $value['modified_by'];
                }
                ?></td>
                <td><?php 
                if(array_key_exists('deleted_at', $value) && !empty($value['deleted_at'])){
                    echo $value['deleted_at'];
                }
                  ?></td>
                
                <td>
                    <a href="show.php?id=<?php echo $value['id'] ;  ?>">View</a>
                    |<a href="edit.php?id=<?php echo $value['id'];  ?>">Edit</a>
                    |<a href="delete.php?id=<?php echo $value['id'];  ?>">Delete</a>
                </td>
            </tr>
            <?php
             }
           }else{
           ?>
            <tr><td colspan="4">
                No data available
                </td>  
            </tr>
            
            <?php
           }
            ?>
        </table>
        |Paging|
    </body>
</html>
