<?php

namespace OroCoder\Crud;

use OroCoder\Utility;

class Category {

    public $name = '';
    public $description = '';
    public $data = array();
    
    private $conn = "";

    function __construct() {
        $this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function all() {

        try {
            $query = "SELECT * FROM categories";

            $_result = $this->conn->query($query);

            foreach ($_result as $row) {
                $this->data[] = $row;
            }

            return $this->data;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        return $this->data;
    }

    public function create() {

        $query = "INSERT INTO `quickqc`.`categories` (`id`, `name`, `description`) VALUES (NULL, '" . $_POST['name'] . "', '" . $_POST['description'] . "')";
        mysql_query($query);

        Utility::redirect();
    }

    public function get() {

        $query = "SELECT * FROM categories WHERE id = " . $_GET['id'];
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);

        return $row;
    }

    public function store() {

        $query = "UPDATE `quickqc`.`categories` SET `name` = '" . $_POST['name'] . "', `description` = '" . $_POST['description'] . "' WHERE `categories`.`id` =" . $_POST['id'];
        mysql_query($query);

        redirect();
    }

    function delete() {

        $query = "DELETE FROM `quickqc`.`categories` WHERE `categories`.`id` = " . $_GET['id'];
        mysql_query($query);

        redirect('http://yahoo.com');
    }

}
