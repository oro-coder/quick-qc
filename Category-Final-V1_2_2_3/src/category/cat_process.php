<?php

//include_once 'applications.php';
include_once("../../vendor/autoload.php");

use OroCoder\category\Category;

if (isset($_POST['cat_name']) && !empty($_POST['cat_name'])) {
    $process_object = new Category();

    $cat_name = $_POST['cat_name'];
    $cat_desc = $_POST['cat_desc'];
    $created_date = $_POST['created_date'];
    $created_by = $_POST['created_by'];
//    debug($_POST);
    $process_object->create($cat_name, $cat_desc, $created_date, $created_by);
} else {
    header('location:../manage_category.php');
}


