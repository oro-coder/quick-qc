<?php
//include_once 'src/category/applications.php';
include_once("vendor/autoload.php");

use OroCoder\category\Category;

$object = new Category;
$data = $object->findone($_GET['id']);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Checklist | Manage Category</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login body_color" >
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- START MENU OR HEADER OPTION -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="login.html">Checklist</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="header_menu">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                            <li><a href="guideline.html">GUIDELINE</a></li>
                            <li><a href="login.html">LOGOUT</a></li>
                            <li><a href="glossary.html">GLOSSARY</a></li>

                        </ul>
                    </div>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- END HEADER -->
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">
                        <!-- SIDEBAR USERPIC -->
                        <div class="profile-userpic">
                            <img src="img/chayan.JPG" class="img-responsive" alt="">
                        </div>
                        <!-- END SIDEBAR USERPIC -->
                        <!-- SIDEBAR USER TITLE -->
                        <div class="profile-usertitle">
                            <div class="profile-usertitle-name">
                                CHAYAN ROY
                            </div>
                            <div class="profile-usertitle-job">
                                Web Developer & Designer
                            </div>
                        </div>
                        <!-- END SIDEBAR USER TITLE -->
                        <!-- SIDEBAR BUTTONS -->
                        <div class="profile-userbuttons">
                            <a href="#"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i></a>
                            <a href="#"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i></a>
                            <a href="#"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i></a>
                            <a href="#"><i id="social" class="fa fa-envelope-square fa-3x social-em"></i></a>
                        </div>
                        <!-- END SIDEBAR BUTTONS -->
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li class="active">
                                    <a href="user_profile.html">
                                        <i class="fa fa-home fa-4x"></i>
                                        Dashboard </a>
                                </li>
                                <li>
                                    <a href="account_setting.html">
                                        <i class="fa fa-cog"></i>
                                        Account Settings </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal12">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Add Project</a>
                                </li>
                                <li>
                                    <a href="manage_project.html">
                                        <i class="fa fa-list"></i>
                                        Manage Project</a>
                                </li>

                                <li>
                                    <a href="manage_category.php">
                                        <i class="fa fa-list"></i>
                                        Manage Category</a>
                                </li>
                                <li>
                                    <a href="manage_qc.html">
                                        <i class="fa fa-tags"></i>
                                        Manage QC</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal1">
                                        <i class="fa fa-spinner"></i>
                                        Suggest QC</a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content" >
                        <div class="clearfix"></div>
                        <hr>
                        <a href="manage_category.php"><button type="button" class="btn btn-default">Back to List</button></a>
                        <a href="manage_category.php"><button type="button" class="btn btn-default">Add New Category</button></a><br/><br/>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th> ID </th>
                                    <th>Category Name</th>
                                    <th>Description</th>
                                    <th>Create Date</th>
                                    <th>Last Modified Date</th>
                                    <th>Created By</th>
                                    <th>Modified By</th>
                                    <th>Action</th>
                                </tr>
                                <?php
                                if (isset($data) && !empty($data)) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if (array_key_exists('id', $data)) {
                                                echo $data['id'];
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('cat_name', $data)) {
                                                echo $data['cat_name'];
                                            } else {
                                                echo "Not Available";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('cat_desc', $data)) {
                                                echo $data['cat_desc'];
                                            } else {
                                                echo "Not Available";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('created_date', $data)) {
                                                echo $data['created_date'];
                                            } else {
                                                echo "Not Available";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('modified_date', $data) && $data['modified_date'] !== "0000-00-00") {
                                                echo $data['modified_date'];
                                            } else {
                                                echo "Not modified yet";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('created_by', $data)) {
                                                echo $data['created_by'];
                                            } else {
                                                echo "Not Available";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if (array_key_exists('modified_by', $data) && !empty($data['modified_by'])) {
                                                echo $data['modified_by'];
                                            } else {
                                                echo " Not modified Yet";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="edit_category.php?id=<?php echo $data['id'] ?>"> Edit</a> |
                                            <a href="src/category/delete_category.php?id=<?php echo $data['id'] ?>"> Delete</a> 
                                        </td>
                                    </tr>
                                    <?php
                                } else {
                                    header('location:manage_category.php');
                                }
                                ?>

                            </table>
                        </div>
                    </div>

                </div> 

            </div>
        </div>
        <br/>
        <br/>

        <div class="footer">
            <footer class="copyright navbar navbar-default navbar-bottom">
                <span>2015 © CHECKLIST TEAM. ALL RIGHT RESERVE.</span>
            </footer>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Suggest QC</h4>
                    </div>
                    <div class="modal-body">
                        <label>Select Your QC Category</label>
                        <select class="form-control">
                            <option>body</option>
                            <option>footer</option>
                            <option>header</option>
                            <option>menu</option>
                            <option>content</option>
                        </select>
                        <br/>
                        <form class="form-horizontal">

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">YOUR QC</label>
                                <div class="col-sm-7">
                                    <input type="text" name="user_qc" class="form-control" id="inputEmail3" placeholder="enter your qc">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--suggest QC javascript popover end here-->
    </body>
    <!-- END BODY -->
</html>
